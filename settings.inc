<?php

// This file is loaded by Drupal's settings.php. Use an IIFE to create a local
// scope and therefore avoid all side effects other than to modify $databases.
(function () use (&$databases) {

  // Returns the converted values for a single connection.
  function convert($connection) {
    $connection['driver'] ??= 'mysql';
    if ($connection['driver'] === 'mysql') {
      $connection['namespace'] = 'Drupal\\mysql57\\Driver\\Database\\mysql';
      // @todo Make this dynamic in case the module is in a different
      //   location.
      $connection['autoload'] = 'modules/contrib/mysql57/src/Driver/Database/mysql/';
      $connection['dependencies'] = [
        'mysql' => [
          'namespace' => 'Drupal\\mysql',
          'autoload' => 'core/modules/mysql/src/',
        ],
      ];
    }
    return $connection;
  }

  $databases['default']['default'] ??= [];
  foreach (array_keys($databases) as $key) {
    foreach (array_keys($databases[$key]) as $target) {
      // This is a single connection.
      if (!is_numeric(array_key_first($databases[$key][$target]))) {
        $databases[$key][$target] = convert($databases[$key][$target]);
      }
      // This is an array of "replica" connections, for load balancing.
      else {
        foreach ($databases[$key][$target] as $index => $target_replica) {
          $databases[$key][$target][$index] = convert($databases[$key][$target][$index]);
        }
      }
    }
  }

}
)();